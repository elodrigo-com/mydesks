package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"runtime"
	"sort"
	"time"
)

var (
	DeskDB		*mongo.Database
	Cfg		FriendConfig
)

type FriendConfig struct {
	PythonPath	string	`yaml:"pythonPath"`
	MongoName	string	`yaml:"mongoName"`
	Programs	[]Program `yaml:"prog"`
}

type Program struct {
	ID			int		`yaml:"id"`
	ProgramName	string	`yaml:"prog_name"`
	Script		string	`yaml:"script"`
	ProgDB		*mongo.Database
}

type Element struct {
	ProgramName string	`bson:"prog_name"`
	TimeOpened	int64	`bson:"time_opened"`
}

func init()  {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	fmt.Println("Initializing...")
	myOS := runtime.GOOS
	fmt.Println("OS found:", myOS)

	fmt.Println("reading yaml files")
	fileOS := "deskfriends_" + myOS + ".yml"
	filename, _ := filepath.Abs(os.Getenv("MY_CONFIG_PATH") + "/" + fileOS)
	yamlFile, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(yamlFile, &Cfg)
	if err != nil {
		panic(err)
	}
}

func main()  {

	fmt.Println("Connecting to MongoDB")
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Panic(err)
	}
	if client != nil {
		err = client.Connect(context.Background())
		if err != nil {
			log.Panic(err)
		}
		DeskDB = client.Database(Cfg.MongoName)
		fmt.Println("Server Started")
	}

	// Hard copy slice and sort it in ascending order
	newList := append(make([]Program, 0, len(Cfg.Programs)), Cfg.Programs...)
	sort.Slice(newList, func(i, j int) bool {
		return newList[i].ID < newList[j].ID
	})
	Cfg.Programs = newList

	// Creates number of goroutines equal to length of Cfg.Programs
	for x := range Cfg.Programs {
		// Each goroutines will find its own program name in mongodb.
		// Each will have its own db object. So, number of mongodb connection will also be equal to length of Programs
		var program = Program {
			ID: 			Cfg.Programs[x].ID,
			ProgramName: 	Cfg.Programs[x].ProgramName,
			Script: 		Cfg.Programs[x].Script,
			ProgDB:			DeskDB,
		}
		go func(p Program) {
			filter := bson.M{"prog_name": p.ProgramName}
			progColl := p.ProgDB.Collection("prog_list")
			for {
				res := progColl.FindOne(context.TODO(), filter)
				var data Element
				err = res.Decode(&data)
				if err != nil {
					// check if err message means it's empty
					fmt.Println(err)
					// here send channel to end goroutine
					break
				}
				current := time.Now().Unix()
				// this means program is running and there's script to run
				if p.Script != "" && current-data.TimeOpened < 60*2 {
					fmt.Println("running command:", p.Script)
					cmd := exec.Command(Cfg.PythonPath, p.Script)
					err = cmd.Run()
					if err != nil {
						fmt.Println(err.Error())
					}
					fmt.Println("command finished:", p.Script)
					fmt.Println("Wait 3 minutes to continue finding", p.ProgramName)
					time.Sleep(time.Minute * 3)
				} else {
					log.Println("tik tok")
					time.Sleep(time.Second * 2)
				}
			}

		}(program)
	}

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	<-ch
	fmt.Println("Stopping Program")
	fmt.Println("Closing MongoDB Connection")
	err = client.Disconnect(context.Background())
	if err != nil {
		log.Panic(err.Error())
	}
	fmt.Println("End of Program")
}
