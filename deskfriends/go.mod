module deskfriends

go 1.13

require (
	github.com/mitchellh/go-ps v1.0.0
	go.mongodb.org/mongo-driver v1.3.0
	gopkg.in/yaml.v2 v2.2.2
)
