package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandleIsProcess(t *testing.T) {
	// Setup initial db
	var DeskConn *mongo.Database
	fmt.Println("Connecting to MongoDB")
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	DeskConn = client.Database("desk_progdb")

	r := mux.NewRouter()
	r.Methods("GET").Path("/is_process_running").HandlerFunc(HandleIsProcess(&MyProgObject{DeskDB:DeskConn}))

	wrt := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/is_process_running", nil)
	r.ServeHTTP(wrt, req)

	log.Println(wrt.Code, wrt.Body)
	err = client.Disconnect(context.Background())
	if err != nil {
		log.Fatal(err)
	}
}