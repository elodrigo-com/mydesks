package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"time"
)

var (
	Port = ":9898"
)

type MyProgObject struct {
	DeskDB		*mongo.Database	`json:"-"`
	TimeOpened	int64			`json:"time_opened" bson:"time_opened"`
	MyProgList	[]*MyProg		`json:"prog_list"`
}

type MyProg struct {
	ProgName	string	`json:"prog_name" bson:"prog_name"`
	TimeOpened	int64	`json:"time_opened" bson:"time_opened"`
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func main() {
	var err error
	var DeskConn *mongo.Database

	fmt.Println("Connecting to MongoDB")
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}
	if client != nil {
		err = client.Connect(context.Background())
		if err != nil {
			log.Fatal(err)
		}
		DeskConn = client.Database("desk_progdb")
	}
	defer func() {
		if err = client.Disconnect(context.Background()); err != nil {
			log.Fatal(err)
		}
	}()

	r := mux.NewRouter()
	r.Methods("GET").Path("/is_process_running").HandlerFunc(HandleIsProcess(&MyProgObject{DeskDB:DeskConn}))

	fmt.Println("my local server started...")
	err = http.ListenAndServe(Port, r)
	if err != nil {
		log.Panic(err)
	}
}

func HandleIsProcess(prog *MyProgObject) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error

		progColl := prog.DeskDB.Collection("prog_list")
		cur, err := progColl.Find(context.TODO(), bson.D{})
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		prog.CleanMyProgObject()

		for cur.Next(context.TODO()) {
			var elem = &MyProg{}
			err = cur.Decode(&elem)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			if time.Now().Unix() - elem.TimeOpened > 60 {
				continue
			} else {
				prog.MyProgList = append(prog.MyProgList, elem)
			}
		}

		if err = cur.Err(); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = cur.Close(context.TODO())
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if len(prog.MyProgList) == 0 {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		output, err := json.Marshal(prog)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		_, err = w.Write(output)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadGateway)
			return
		}
		return
	}
}

func (p *MyProgObject) CleanMyProgObject() {
	for i:=0; i<len(p.MyProgList); i++ {
		p.MyProgList[i].ProgName = ""
		p.MyProgList[i].TimeOpened = 0
	}
}

