module mydesks/localapi

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	go.mongodb.org/mongo-driver v1.3.0
	go.uber.org/zap v1.13.0
)
