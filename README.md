# MyDesks
* This project includes the applications run on a development computer to improve the desktop program maintenance efficiency.
#### Configuration
* Set 'MY_CONFIG_PATH' as environment variable for yaml configuration file directory.
* Refer [name]Config struct for yaml configuration.
    - 'mongoName' field is used for main database name through out this project.
    - yaml filename: {name}_{os_name}.yml
        + os_name: windows, darwin, or linux
    
#### Dependencies 
* MongoDB installation required.
* go version 1.13 +
* go modules will install packages when build or run test

# Applications

## 1. Process Finder
* Run this application on the background of windows or macOS will check if programs that listed in a configuration file are currently running on the system.

## 2. Local API
* Backend for local desktop. Handles request from frontend.

## 3. Desktop Friend
* Run this application on the background of windows or macOS will trigger python script when a related process is opened.
* Information about the related process and its scripts to run are stored in mongodb.
* The related process's status is updated by Process Finder.
##### Requirement
* Process Finder must be running.
* It is required to number each program in the id field of prog array. When the program initializes, it will sort the list in struct by the id. This ID field is pretty relevant in this application because storing a list of programs in the order may prevent fatal error when executing dense scripts concurrently in a loop.
##### etc
* It doesn't find the process running on the operating system by itself. It only looks up MongoDB and checks the time_opened field to know about the process existence. 