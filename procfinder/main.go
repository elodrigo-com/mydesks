package main

import (
	"context"
	"fmt"
	"github.com/mitchellh/go-ps"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"time"
)

var (
	DeskDB		*mongo.Database
	ProcList	[]string
	ProcCfg		ProcConfig
)

type ProcConfig struct {
	Proc	[]struct {
		ProcName	string	`yaml:"procName"`
		Windows		string	`yaml:"windows"`
		Mac			string	`yaml:"mac"`
		Linux		string	`yaml:"linux"`
	}	`yaml:"proc"`
	MongoName	string	`yaml:"mongoName"`
}


func init()  {
	fmt.Println("Initializing...")
	myOS := runtime.GOOS
	fmt.Println("OS found:", myOS)

	fmt.Println("reading yaml files")
	filename, _ := filepath.Abs(os.Getenv("MY_CONFIG_PATH") + "/procfinder.yml")
	yamlFile, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(yamlFile, &ProcCfg)
	if err != nil {
		panic(err)
	}

	if myOS == "windows" {
		for win := range ProcCfg.Proc {
			ProcList = append(ProcList, ProcCfg.Proc[win].Windows)
		}
	} else {
		for mac := range ProcCfg.Proc {
			ProcList = append(ProcList, ProcCfg.Proc[mac].Mac)
		}
	}
}

func main()  {
	var err error
	fmt.Println("Connecting to MongoDB")
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		panic(err)
	}
	if client != nil {
		err = client.Connect(context.Background())
		if err != nil {
			panic(err)
		}
		DeskDB = client.Database(ProcCfg.MongoName)
		fmt.Println("Server Started")
	}

	go RunMyFinder()

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	<-ch
	fmt.Println("Stopping Program")
	fmt.Println("Closing MongoDB Connection")
	err = client.Disconnect(context.Background())
	if err != nil {
		log.Panic(err.Error())
	}
	fmt.Println("End of Program")
}

func RunMyFinder() {
	fmt.Println("start looking for my programs")
	progColl := DeskDB.Collection("prog_list")
	opts := options.Update().SetUpsert(true)
	for {
		processList, err := ps.Processes()
		if err != nil {
			log.Println(err)
		}

		for x := range processList {
			var process ps.Process
			process = processList[x]
			//log.Printf("%d\t%s\n", process.Pid(), process.Executable())

			for y := range ProcList {
				if process.Executable() == ProcList[y] {
					_, err = progColl.UpdateOne(context.TODO(), bson.M{"prog_name": ProcList[y]}, bson.M{"$set": bson.M{"time_opened": time.Now().Unix()}}, opts)
					log.Println(process.Executable())
				}
			}
		}
		time.Sleep(time.Second * 2)
	}
}
