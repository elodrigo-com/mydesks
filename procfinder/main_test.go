package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"os/signal"
	"testing"
)

func TestRunMyFinder(t *testing.T) {
	fmt.Println("Connecting to MongoDB")
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Panic(err)
	}
	if client != nil {
		err = client.Connect(context.Background())
		if err != nil {
			log.Panic(err)
		}
		DeskDB = client.Database("desk_progdb")
		fmt.Println("Server Started")
	}

	go RunMyFinder()

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	<-ch
	fmt.Println("Stopping Program")
	fmt.Println("Closing MongoDB Connection")
	err = client.Disconnect(context.Background())
	if err != nil {
		log.Panic(err.Error())
	}
	fmt.Println("End of Program")
}
